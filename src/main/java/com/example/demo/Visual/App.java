package com.example.demo.Visual;

import com.example.demo.dominio.Cliente;
import com.example.demo.dominio.ClienteRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by CLIENTE on 21/07/2017.
 */

@SpringUI
public class App extends UI {
    @Autowired
    ClienteRepository clienteRepository;//

    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        Grid<Cliente> grid = new Grid<>();

        TextField nombre = new TextField("Nombre");
        TextField apellido = new TextField("Apellido");
        TextField dpi = new TextField("Dpi");

        Button add = new Button("REGISTRAR");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Cliente c = new Cliente();

                c.setNombre(nombre.getValue());
                c.setApellido(apellido.getValue());
                c.setDpi(Integer.parseInt(dpi.getValue()));
                clienteRepository.save(c);
                grid.setItems(clienteRepository.findAll());

                nombre.clear();
                apellido.clear();
                dpi.clear();
                Notification.show("Cliente registrado");

            }

        });


        hlayout.addComponents(nombre, apellido, dpi,  add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);


        grid.addColumn(Cliente::getId).setCaption("Id");
        grid.addColumn(Cliente::getNombre).setCaption("Nombre");
        grid.addColumn(Cliente::getApellido).setCaption("Apellido");
        grid.addColumn(Cliente::getDpi).setCaption("Dpi");

        layout.addComponents(hlayout);
        layout.addComponents(grid);

        setContent( layout );

    }

}


