package com.example.demo.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by CLIENTE on 21/07/2017.
 */
@Entity
public class Cliente {

    private String nombre;
    private String apellido;
    private int dpi;

    private Long id;

    public Cliente() {
        this.nombre = nombre;
    }
    @Id@GeneratedValue(strategy = GenerationType.AUTO)

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Cliente(String nombre, String apellido, int dpi, Long id) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dpi = dpi;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getDpi() {
        return dpi;
    }

    public void setDpi(int dpi) {
        this.dpi = dpi;
    }
}
