package com.example.demo.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by CLIENTE on 21/07/2017.
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
}
